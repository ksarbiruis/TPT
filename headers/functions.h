//
// Created by Борис Кожуро on 21.02.2020.
//
#include <iostream>
#include <list>
#include <sstream>

#ifndef TP_TEST_FUNCTIONS_H
#define TP_TEST_FUNCTIONS_H

int PrintMenu ();

void PrintList(std::list<std::string>&);
void PrintElementOfList(std::list<std::string>&, int);
void EditElementOfList(std::list<std::string>&, int);
void DeleteElementOfList(std::list<std::string>&, int);
void ElementCase (std::list<std::string>& , void (*ElementAction) (std::list<std::string>& , int));


#endif //TP_TEST_FUNCTIONS_H
