cmake_minimum_required(VERSION 3.15)
project(TP_Test)

set(CMAKE_CXX_STANDARD 14)

add_executable(TP_Test main.cpp functions.cpp headers/functions.h)