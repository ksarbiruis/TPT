//
// Created by Борис Кожуро on 21.02.2020.
//
#include "headers/functions.h"

int PrintMenu () {
    std::cout<< "1: ввести  строку или добавить поля \n"<<
             "2: вывести все поля\n"<<
             "3: вывести поле по номеру\n"<<
             "4: редактировать поле\n"<<
             "5: удалить поле\n"<<
             "6: очистить\n" <<
             "0: выход\n";
    int result;
    std::cin >> result;
    return result;
}

void PrintList(std::list<std::string>& List) {
    std::list<std::string> ::iterator ListIt;
    int i=0;
    for(ListIt = List.begin(); ListIt!= List.end(); ++ListIt)
    {
        std::cout << i++ << ": "<<*ListIt<<"\n";
    }
    std::cout<<std::endl;
}
void PrintElementOfList(std::list<std::string>& List, int pos) {
    std::list<std::string> ::iterator ListIt;
    int i=0;
    for(ListIt = List.begin(); i < pos;++ListIt)
    {
        i++;
    }
    std::cout << i++ << ": "<<*ListIt<<"\n";
}
void EditElementOfList(std::list<std::string>& List, int pos) {
    std::list<std::string> ::iterator ListIt;
    int i=0;
    for(ListIt = List.begin(); i < pos;++ListIt)
    {
        i++;
    }
    std::cout << i++ << ": "<<*ListIt<<"\n"<<"заменить на:\t";
    std::string temp;
    std::cin>>temp;
    std::cin.ignore();
    List.insert(ListIt,temp);
    List.erase(ListIt);
}
void DeleteElementOfList(std::list<std::string>& List, int pos) {
    std::list<std::string> ::iterator ListIt;
    int i=0;
    for(ListIt = List.begin(); i < pos;++ListIt)
    {
        i++;
    }
    std::cout << i++ << ": "<<*ListIt<<"\t"<<"будет удален\n";
    List.erase(ListIt);
}

void ElementCase (std::list<std::string>& DividedStr, void (*ElementAction) (std::list<std::string>& , int)){
    if (!DividedStr.empty()) {
        int pos = 0;
        std::cout<< "введите номер\t";
        std::cin >> pos;
        if (pos >= 0 && pos < DividedStr.size())
            ElementAction(DividedStr,pos);
        else
            std::cout<<"за границей!\n";
    } else {
        std::cout << "empty!\n";
    }
}