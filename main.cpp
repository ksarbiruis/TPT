#include "headers/functions.h"


int main() {
    std::string TempStr;
    int Choice = 0;
    std::list<std::string> DividedStr;
    while(true) {
        Choice = PrintMenu();
        std::cin.ignore();
        switch(Choice) {
            case 1: {
                std::cout<<"ввод:";
                std::getline(std::cin, TempStr, '\n');
                std::istringstream inp(TempStr);
                    while (std::getline(inp, TempStr, '\t')) {
                        DividedStr.push_back(TempStr);
                    }
                    break;
            }
            case 2: {
                if (!DividedStr.empty()) {
                    PrintList(DividedStr);
                } else {
                    std::cout << "пусто!\n";
                }
                break;
            }
            case 3: {
                ElementCase(DividedStr,PrintElementOfList);
                break;
            }
            case 4:
                ElementCase(DividedStr,EditElementOfList);
                break;
            case 5:
                ElementCase(DividedStr,DeleteElementOfList);
                break;
            case 6:
                DividedStr.clear();
                break;
            case 0: {
                return 0;
            }
        }
    }
}